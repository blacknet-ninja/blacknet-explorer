



const genesis = require('./genesis.json');

const bech32_addr = require('./account/addr');



function getAddress(pbkey) {


    return bech32_addr.encode('blacknet', Buffer.from(pbkey, 'hex'));
}

async function processGenesis() {

    for (let g of genesis) {

        let address = getAddress(g.publicKey);
        let instance = {
            address,
            balance: g.balance,
            confirmedBalance: g.balance,
            txamount: 1
        };

        let account = await Account.findOne({address});

        if(!account){
            account = new Account(instance);
            await account.save();

            let tx = {};
            
            tx.txid = '',
            tx.size = 0,
            tx.signature = '',
            tx.from = 'genesis',
            tx.seq = 0,
            tx.blockHash = '0000000000000000000000000000000000000000000000000000000000000000',
            tx.fee = 0,
            tx.type = 999,
            tx.data = {blockHeight: 0,blockHash: tx.blockHash, amount: g.balance};
            tx.to = address;
            tx.amount = g.balance;
            tx.time = 1545555600;
            tx = new Transaction(tx);
            
            await tx.save();
        }
    }
}

processGenesis();


