
var mongoose = require('mongoose');

var mongodb_url = global.process.env.MONGODB_URL ? global.process.env.MONGODB_URL : 'mongodb://localhost/test';

mongoose.connect(mongodb_url, {
    useNewUrlParser: true,
    auto_reconnect: true,
    useUnifiedTopology: true,
    poolSize: 10
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("we're connected!")
});

var ExchangeSchema = new mongoose.Schema({
    price: Object,
    lines: Array,
    time: Number,
    coin: String
});

var BlockSchema = new mongoose.Schema({
    size: Number,
    version: String,
    previous: String,
    time: String,
    generator: String,
    contentHash: String,
    signature: String,
    transactions: Array,
    height: Number,
    blockHash: String,
    reward: String
});

var TransactionSchema = new mongoose.Schema({
    txid: String,
    size: Number,
    signature: String,
    from: String,
    seq: Number,
    blockHash: String,
    fee: Number,
    type: Number,
    data: Object,
    to: String,
    time: String,
    amount: Number,
    blockHeight: Number,
    txowner: Array,
    messageType: String
});


var AccountSchema = new mongoose.Schema({
    address: String,
    balance: { type: Number, default: 0 },
    txamount: { type: Number, default: 0 },
    confirmedBalance: { type: Number, default: 0 },
    stakingBalance: { type: Number, default: 0 },
    seq: Number,
    blocks: { type: Number, default: 0 },
    realBalance: { type: Number, default: 0 },
    outLeasesBalance: { type: Number, default: 0 },
    inLeasesBalance: { type: Number, default: 0 },
    displayName: { type: String, default: null },
    profile: mongoose.Schema.Types.Mixed
});

var PeerSchema = new mongoose.Schema({
    ip: String,
    port: String,
    location: Object
});


var Message = new mongoose.Schema({
    text: String,
    from: String,
    to: String,
    time: String,
    txid: String,
    amount: Number,
    data: Object,
    msgOwner: Array,
    type: String
});

var ChatMap = new mongoose.Schema({
    
    address: String,
    contacts: mongoose.Schema.Types.Mixed, 
    groups: mongoose.Schema.Types.Mixed

    /**
     * 
     * {
     *    'blacknet126avvjlevw9xqnmlgscytxtuf57ndwzmz569vcaatceevhcjxjfq29ywqc': {
     *          time: 22123123,
     *          message: 'ojbk'    
     *    }
     * }
     * 
     */
});




var Feedback = new mongoose.Schema({
    text: String,
    account: String
});

global.Peer = mongoose.model('Peer', PeerSchema);
global.Block = mongoose.model('Block', BlockSchema);
global.Account = mongoose.model('Account', AccountSchema);
global.Transaction = mongoose.model('Transaction', TransactionSchema);
global.Exchange = mongoose.model('Exchange', ExchangeSchema);
global.Feedback = mongoose.model('Feedback', Feedback);
global.Message = mongoose.model('Message', Message);
global.ChatMap = mongoose.model('ChatMap', ChatMap);

global.DataBase = db;