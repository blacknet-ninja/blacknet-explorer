
const Bignumber = require('bignumber.js');


const Router = require('koa-router');
const router = new Router({
    prefix: '/api'
});
const BigNumber = require('bignumber.js');
const API = require('../lib/api');
const Utils = require('../lib/utils');

const request = require("request-promise");

router.get('/networkhashrate', async (ctx, next) => {

    let data = {
        networkhashrate: 0
    };
    let list = await Block.find({}).sort({ height: 'desc' }).select('generator').limit(2880);
    let map = {};

    for (let block of list) {
        map[block.generator] = null;
    }
    let keys = Object.keys(map);
    data.workers = keys.length;

    let accounts = await Account.find({ address: { $in: keys } }).select('stakingBalance');

    for (let account of accounts) {
        data.networkhashrate += account.stakingBalance;
    }

    ctx.body = data;
});

router.get('/alias', async (ctx, next) => {

    let query = ctx.query;
    let res = { status: false, message: 'The Signature does not match the given Address/Name' }

    if (!Utils.verifyAccount(query.account)) {
        res.filed = "account";
    }
    if (!Utils.verifySign(query.signature)) {
        res.filed = "signature";
    }
    if (!Utils.verifyAlias(query.display_name)) {
        res.filed = "display_name";
    }
    res.status = await API.verifySign(query.account, query.signature, query.display_name)
    if (res.status) {
        let account = await Account.findOne({ address: query.account })
        if (account) {
            account.displayName = query.display_name
            account.save();
        } else {
            res.status = false
            res.message = "account not found"
        }
    }
    ctx.body = res;
});

router.get('/nickname/:account', async (ctx, next) => {

    let address = ctx.params.account;
    let query = { from: address, to: address, amount: 1000000, text: { $regex: /^nickname:\ / } };
    let tx = await Message.find(query).limit(1).sort({ 'time': 'desc' });

    ctx.body = tx.length ? tx[0] : {};
});

router.get('/signmessage/:account', async (ctx, next) => {

    let address = ctx.params.account;
    let query = { from: address, to: address, amount: 2000000, text: { $regex: /^signmessage:\ / } };
    let tx = await Message.find(query).limit(1).sort({ 'time': 'desc' })

    ctx.body = tx.length ? tx[0] : {};
});



router.get('/contact/:account', async (ctx, next) => {

    let msgs = await Message.find({ from: ctx.params.account, text: { $regex: /^new\ contact:\ / } })

    let contacts = msgs.map(msg => {
        let contact = {};
        contact.name = msg.text;
        contact.account = msg.to;
        return contact;
    });

    ctx.body = contacts;
});



router.get('/price', async (ctx, next) => {
    // let ex = await Exchange.findOne({coin: "BLN"}).sort({ time: 'desc' });
    // if(ex){
    let ex = await GetEx()
    // }
    ctx.body = {
        lines: ex.lines,
        price: ex.price
    };
});

const poolMap = {
    'blacknet1gqcgrut705k03vwrmvrqhg689gvqxtle4sly434fan09sg0kz2yszqa8ug': {
        displayName: 'stakepool.xyz',
        link: 'http://stakepool.xyz'
    },
    'blacknet126avvjlevw9xqnmlgscytxtuf57ndwzmz569vcaatceevhcjxjfq29ywqc': {
        displayName: 'blnpool.io',
        link: 'https://www.blnpool.io'
    }
};

router.get('/v2/feedback', async (ctx, next) => {

    let data = {
        text: ctx.query.text,
        account: ctx.query.account
    }

    let feedback = new Feedback(data);
    feedback.save();


    ctx.body = {
        code: 0,
        message: 'success'
    };

});


router.get('/v2/block/:height', async (ctx, next) => {

    let txns = [], hash, str = ctx.params.height;
    if (Utils.verifyBlockNumber(str)) {
        let block = await Block.findOne({ height: str }).select('blockHash');
        hash = block.blockHash;
    } else if (Utils.verifyHash(str)) {
        hash = str.toUpperCase();
    }
    if (hash) {
        txns = await Transaction.find({ blockHash: hash });
    }
    ctx.body = txns;
});

router.get('/v2/account/:address', async (ctx, next) => {

    let txns = [], count = 0, page = ctx.query.page || 1, page_number = 0, pager = [];
    let address = ctx.params.address.toLowerCase(), query = { txowner: address };
    let filter_pos_generated = ctx.query.filter_pos_generated || 0;
    let type = ctx.query.type || 'all';
    let isGt10000 = ctx.query.isGt10000;
    if (!Utils.verifyAccount(ctx.params.address)) {
        ctx.body = {
            txns,
            page_number,
            filter_pos_generated, pager: [], page, type
        }
        return;
    }

    if (isGt10000 == 1) {
        query['data.amount'] = { $gt: 10000 * 1e8 };
    }

    if (type != 'all') {

        query.type = type == "genesis" ? 999 : type;

    }
    let txamount = await Transaction.count(query);
    txns = await Transaction.find(query).skip((page - 1) * 100).limit(100).sort({ time: -1 }).lean();

    if (txns.length == 100 || page > 1) {
        count = txamount;
        page_number = Math.ceil(count / 100);
        pager = getPager(+page, page_number);
    }
    ctx.body = {
        txns,
        page_number,
        isGt10000, pager, page, type
    }
});


// for mobile refresh
router.get('/v3/account/:address', async (ctx, next) => {

    let txns = [], page = ctx.query.page || 1;
    let address = ctx.params.address.toLowerCase(), query = { txowner: address };
    let type = ctx.query.type === undefined ? 'all' : ctx.query.type;
    let gt = ctx.query.gt;
    if (!Utils.verifyAccount(ctx.params.address)) {
        ctx.body = {
            txns,
            page, type
        }
        return;
    }

    if (gt > 0) {
        query['data.amount'] = { $gt: gt * 1e9 };
    }

    if (type != 'all') {

        query.type = type == "genesis" ? 999 : type;
    }

    txns = await Transaction.find(query).skip((page - 1) * 100).limit(100).sort({ time: -1 }).lean();

    ctx.body = {
        txns,
        page, type
    }
});

router.get('/recent_blocks', async (ctx, next) => {

    let blocks = await Block.find({}).sort({ height: 'desc' }).limit(50).lean();

    blocks.map(block => {
        if (poolMap[block.generator]) {

            block.map = poolMap[block.generator];
        }
    });

    ctx.body = blocks;
});

router.get('/recent_messages', async (ctx, next) => {
    let msgs = await Message.find({ text: { $not: { $regex: /(profit|Reward|send)/g } } }).limit(1000).sort({ time: 'desc' });
    ctx.body = msgs;
});

router.get('/recent_transactions', async (ctx, next) => {
    let txns = [];
    let rtxns = await Transaction.find({ type: { $not: { $gt: 253 } } }).limit(50).sort({ time: 'desc' });
    ctx.body = { txns, rtxns };
});
async function getTxPool() {

    let txpool = await API.getTxPool();
    let txns = [];

    if (txpool.tx || txpool.tx.length > 0) {
        for (let txhash of txpool.tx) {

            let tx = await API.getTxWithTxHash(txhash);

            if (tx && tx.data) {
                txns.push(tx);
            }
        }
    }
    return txns;
}

router.get('/tx/:hash', async (ctx, next) => {

    if (!Utils.verifyHash(ctx.params.hash)) {
        return next()
    }

    let block = await Block.findOne({}).sort({ height: -1 });
    let hash = ctx.params.hash, query = { txid: hash.toUpperCase() };
    let tx = await Transaction.findOne(query).lean();
    if (!tx) {
        let txpool = await getTxPool();
        if (txpool.length > 0) {
            for (let obj of txpool) {
                if (obj.hash == hash.toUpperCase()) {
                    tx = obj;
                    tx.txid = tx.hash;
                    tx.to = tx.data.to;
                    tx.txpool = true;
                    tx.amount = tx.data.amount;
                    tx.message = tx.data.message;
                    tx.data.blockHeight = block.height;
                }
            }
        }
    }
    if (!tx) {
        
        ctx.body = {status: 'error', msg: 'tx not found'}
        return;

    }
    tx.amountStr = toAmountStr(tx.amount)
    tx.feeStr = toAmountStr(tx.fee)
    tx.typeStr = getTxType(tx.type);
    tx.confirmations = block.height - tx.data.blockHeight + 1;
    ctx.body = tx;
});

router.get('/top_accounts', async (ctx, next) => {

    let order = ctx.query.order_by || 'rich';

    if (order == 'rich') {
        query = { realBalance: 'desc' };
    } else if (order == 'pos') {
        query = { blocks: 'desc' };
    } else {
        query = { stakingBalance: 'desc' };
    }

    ctx.body = await Account.find({}).limit(100).sort(query);;
});

router.get('/top_accounts_chardata', async (ctx, next) => {

    let order = ctx.query.order_by || 'rich', accounts, chartData = [], supply = overview.supply, params = {};

    if (order == 'rich') {
        query = { realBalance: 'desc' };
        params = {
            title: "",
            name: "Balance"
        }
    } else if (order == 'pos') {
        query = { blocks: 'desc' };
        params = {
            title: "",
            name: "Blocks"
        }
        // 计算总量
        let count = await Account.aggregate([{ $match: { blocks: { $gt: 0 } } }, { $group: { _id: null, totalBlocks: { $sum: "$blocks" } } }])
        if (count[0] && count[0].totalBlocks) {
            supply = count[0].totalBlocks
        }
    } else {
        query = { stakingBalance: 'desc' };
        params = {
            title: "",
            name: "Balance"
        }
        // 计算总量
        let count = await Account.aggregate([{ $match: { stakingBalance: { $gt: 0 } } }, { $group: { _id: null, stakingBalance: { $sum: "$stakingBalance" } } }])
        if (count[0] && count[0].stakingBalance) {
            supply = count[0].stakingBalance
        }
    }

    accounts = await Account.find({}).limit(100).sort(query);
    // accounts = accountser(accounts);

    chartData = formartAccountChartData(accounts, supply, order)

    ctx.body = {
        chart: chartData,
        params: params
    };
});


function formartAccountChartData(accounts, supply, type) {
    var chartdata = []
    supply = new BigNumber(supply)
    var total = new BigNumber(0)
    accounts.map(account => {
        switch (type) {
            case "rich":
                var balance = new BigNumber(account.balance).dividedBy(1e8)
                chartdata.push([account.address, balance.toNumber()])
                total = BigNumber.sum.apply(null, [total, balance])
                break;
            case "pos":
                var blocks = new BigNumber(account.blocks)
                chartdata.push([account.address, blocks.toNumber()])
                total = BigNumber.sum.apply(null, [total, blocks])
            default:
                var stakingBalance = new BigNumber(account.stakingBalance)
                chartdata.push([account.address, stakingBalance.toNumber()])
                total = BigNumber.sum.apply(null, [total, stakingBalance])
                break;
        }
    });
    chartdata.push(["Other", parseFloat(supply - total)])
    return chartdata
}


router.get('/block_genesis', async (ctx, next) => {
    let block = {
        size: 34100,
        blockHash: "I see potential to scale better than Ethereum and cheaper than EOS",
        height: 0,
        version: "1",
        previous: "The Blackcoin Project",
        time: "1545555600",
        generator: "000000000000000000000000000000000000000000000000000000000000000",
        contentHash: "000000000000000000000000000000000000000000000000000000000000000",
        signature: new Array(100).join('0'),
        reward: "1,000,000,000",
    };
    block.transactions = await Transaction.find({ type: 999 });
    block.transactions = block.transactions.sort(function (x, y) {
        return y.amount - x.amount;
    });
    ctx.body = block;
});



router.get('/account/ledger/:address', async (ctx, next) => {

    let address = ctx.params.address.toLowerCase()
    let account = await Account.findOne({ address: address });

    if (!account) {
        account = {
            balance: 0,
            address,
            confirmedBalance: 0,
            stakingBalance: 0,
            txamount: 0
        }
    }
    account.txamount = await Transaction.countDocuments({ txowner: address });

    ctx.body = account;
});


router.get('/user/:account', async (ctx, next) => {

    
    let user = await Account.findOne({ address: ctx.params.account});
    ctx.body = user || {profile: {}};
});

router.get('/account/txns/:address', async (ctx, next) => {

    let page = ctx.query.page || 1;
    let address = ctx.params.address.toLowerCase(), query = { $or: [{ from: address }, { to: address }] };
    let type = ctx.query.type;

    if (type !== undefined) {
        query.type = +type;
    }

    ctx.body = await Transaction.find(query).skip((page - 1) * 100).sort({ time: -1 }).limit(100);;
});

router.get('/pool/:address', async (ctx, next) => {

    let page = ctx.query.page, type = ctx.query.type;
    let address = ctx.params.address.toLowerCase(), query = { $or: [{ from: address }, { to: address }] };

    if (type) {
        query.type = type;
    }

    ctx.body = await Transaction.find(query).skip((page - 1) * 100).limit(100).sort({ time: -1 }).lean();;
});

router.get('/overview', async (ctx, next) => {

    ctx.body = global.overview;
});

router.get('/statistics', async (ctx, next) => {

    let transactions = await Transaction.estimatedDocumentCount();
    let accounts = await Account.estimatedDocumentCount();

    let payments = await Transaction.countDocuments({ type: 0 });
    let leases = await Transaction.countDocuments({ type: 2 });
    let cancelleases = await Transaction.countDocuments({ type: 3 });
    let bapp = await Transaction.countDocuments({ type: 4 });

    let overview = await initOverview();

    ctx.body = {
        transactions,
        accounts,
        payments,
        leases,
        cancelleases,
        bapp,
        overview
    };
});


async function initOverview() {
    let ledger = await API.getInfo();
    let nodeInfo = await API.getNodeInfo();
    ledger.supply = new Bignumber(ledger.supply).dividedBy(1e8).toFixed(0);
    return overview = {
        ...ledger,
        ...nodeInfo
    };
}

router.get('/incomingleases/:account', async (ctx, next) => {

    ctx.body = await leases(ctx.params.account, 'to');
});

router.get('/forkV2', async (ctx, next) => {

    let blocks = await Block.find({ height: { $gt: 479180 } }).select('version');
    let forkV2 = 0, MATURITY = 1350;
    for (let block of blocks) {

        if (block.version == 2) {
            forkV2 = Math.min(forkV2 + 1, MATURITY + 1);
        } else {
            forkV2 = Math.max(forkV2 - 1, 0);
        }
    }
    ctx.body = {
        forkV2
    };
});

router.get('/wallet/releases', async (ctx, next) => {

    ctx.body = {
        version: '0.2.5',
        download: 'https://vasin.nl/blacknet-0.2.5.zip'
    };
});

router.get('/outleases/:account', async (ctx, next) => {

    ctx.body = await leases(ctx.params.account, 'from');
});

module.exports = router;


async function leases(account, type) {

    let query = { type: { $in: [2, 3] } }, txns, leases = {};

    query[type] = account.toLowerCase();
    txns = await Transaction.find(query);

    for (let tx of txns) {
        let h = tx.data.blockHeight, address = type == 'from' ? tx.to : tx.from;
        if (leases[address] == undefined) {
            leases[address] = {};
        }
        if (!leases[address][h]) {
            leases[address][h] = 0;
        }
        if (tx.type == 2) {
            leases[address][h] += tx.amount;
        } else {
            let targetHeight = tx.data.height;
            leases[address][targetHeight] -= tx.amount;
        }

    }
    let list = [];
    for (let key in leases) {

        for (let k in leases[key]) {

            let amount = leases[key][k];
            if (amount > 0) {
                let item = {};
                item.height = +k;
                item.publicKey = key;
                item.amount = amount;
                list.push(item);
            }
        }

    }

    return list;

}

/**
 * format amount to str 
 * @method toAmountStr
 * @param {number} amount
 * @return {string} str
 */
function toAmountStr(amount) {
    return new BigNumber(amount).dividedBy(1e8).toFixed(8);
}


var txType = [
    "Transfer",
    "Burn",
    "Lease",
    "CancelLease",
    "BApp",
    "CreateSwap"];
txType[7] = "RefundSwap";
txType[9] = "CreateMultisig";
txType[10] = "SpendMultisig";
txType[11] = "WithdrawFromLease";
txType[12] = "ClaimSwap";
txType[16] = "Batch";
txType[254] = "PoS Generated";
txType["all"] = "All";

function getTxType(type) {

    return txType[type] || 'genesis';
}

function getPager(now, max) {

    let arr = [now - 3, now - 2, now - 1, now, now + 1, now + 2, now + 3];

    arr = arr.filter((page) => {

        if (page < 1) return false;

        if (page > max) return false;

        return true;
    });

    return arr;
}


async function GetEx() {
    let ex = await Exchange.findOne({ coin: "BLN" }).sort({ time: 'desc' });
    if (!ex) {
        ex = new Exchange({ coin: "BLN", lines: [], price: {} })
    }
    let body = await new Promise((resolve) => {
        request({
            url: "https://www.citex.co.kr/quot/queryCandlestick?symbol=81&range=86400000",
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Mobile Safari/537.36',
                "Referer": "https://www.citex.co.kr/"
            },
            forever: true,
            json: true
        }, (err, res, body) => {
            resolve(body);
        });
    });
    if (body && body.success && body.data.lines.length) {
        let lines = []
        body.data.lines.map((line) => {
            lines.push([line[0], line[4]])
        })
        if (lines.length) {
            ex.lines = lines
        }
    }
    body = await new Promise((resolve) => {
        request({
            url: "https://www.citex.co.kr/quot/queryIndicatorList",
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Mobile Safari/537.36',
                "Referer": "https://www.citex.co.kr/"
            },
            forever: true,
            json: true
        }, (err, res, body) => {
            resolve(body);
        });
    });
    if (body && body.length) {
        for (const key in body) {
            if (body.hasOwnProperty(key)) {
                if (body[key]["contractId"] == 81) {
                    ex.price[body[key].symbol] = {
                        "price": body[key].lastPrice,
                        "percent": new BigNumber(body[key].priceChangeRadio).times(1e2).toFixed(2)
                    }
                    if (body[key].contractVos.length) {
                        body[key].contractVos.map((p) => {
                            ex.price[p.symbol] = {
                                "price": p.lastPrice,
                                "percent": new BigNumber(p.priceChangeRadio).times(1e2).toFixed(2)
                            }
                        })
                    }
                    break;
                }
            }
        }
    }
    ex.save();
    return ex
}


router.get('/account/:address', async (ctx, next) => {
    let start = Date.now()
    if (!Utils.verifyAccount(ctx.params.address)) {
        return next()
    }
    let txns = [], count = 0, page = ctx.query.page || 1, page_number = 0, pager = [];
    let address = ctx.params.address.toLowerCase(), query = { $or: [{ from: address }, { to: address }] };
    let filter_pos_generated = ctx.query.filter_pos_generated || 0;
    let type = ctx.query.type || 'all';
    let account = await Account.findOne({ address: address });


    console.log(`#1 time is ${Date.now() - start}ms`)
    if (type != 'all') {
        if (type == "genesis") {
            query.type = 999
        } else {
            query.type = type
        }
    } else {
        if (filter_pos_generated == '1') {
            query.type = { $not: { $eq: 254 } };
        }
    }

    if (!account) {
        account = {
            balance: 0,
            address,
            confirmedBalance: 0,
            stakingBalance: 0,
            txamount: 0
        }
    } else {
        txns = await Transaction.find(query).skip((page - 1) * 100).limit(100).sort({ time: -1 }).lean();
        account.txamount = await Transaction.countDocuments(query);
    }

    if (txns.length == 100 || page > 1) {
        count = account.txamount;
        page_number = Math.ceil(count / 100);
        pager = getPager(+page, page_number);
    }
    ctx.body = {
        txns,
        page_number,
        filter_pos_generated, pager, page, type
    }
});



router.get('/block/:height', async (ctx, next) => {

    let block;
    if (Utils.verifyBlockNumber(ctx.params.height)) {
        block = await Block.findOne({ height: ctx.params.height });
    } else if (Utils.verifyHash(ctx.params.height)) {
        block = await Block.findOne({ blockHash: String(ctx.params.height).toUpperCase() });
    } else {
        return next()
    }

    if (!block) {
        return await ctx.render('404', { msg: "Block not found" })
    }
    block.transactions = await Transaction.find({ blockHash: block.blockHash });
    ctx.body = block;
});






// for heart 
router.get('/lastblock', async (ctx, next) => {

    ctx.body = await Block.findOne({}).sort({ height: 'desc' });
});


router.get('/users', async (ctx, next) => {

    
    let users = await Account.find({ 'profile.nickname': {$exists: true}});
    ctx.body = users;
});


// for 论坛


router.get('/post', async (ctx, next) => {

    let query = { 'amount': 3000000, 'data.replyid': {$exists: false}};
    let messages = await Message.find(query).limit(100).sort({ 'data.lastreply': -1 });

    ctx.body = messages;
});


router.get('/post/list/:txid', async (ctx, next) => {
    let txid = ctx.params.txid;
    let query = { 'amount': 310000 , 'data.replyid': txid, 'data.quote': {$exists: false}};
    let messages = await Message.find(query).limit(100).sort({ 'time': 1 });
    
    ctx.body = messages;
});

router.get('/portfolio/:account', async (ctx, next) => {
    let account = ctx.params.account;
    let query = { 'amount': 10000000 , type: 'portfolio', from: account, to: account};
    let msg = await Message.findOne(query).sort({ 'time': -1 });
    
    ctx.body = msg || {};
});

router.get('/post/comment/:txid', async (ctx, next) => {
    let txid = ctx.params.txid;
    let query = { 'amount': 310000 , 'data.quote': txid};
    let messages = await Message.find(query).sort({ 'time': 1 });
    
    ctx.body = messages;
});

// for chat service


router.get('/chatmap/v2/:account', async (ctx, next) => {

    let chatmap = await ChatMap.findOne({ address: ctx.params.account}).lean();
    
    let txids ;

    if(chatmap.groups){
        txids = Object.keys(chatmap.groups);

        if(txids.length){
            
            for(let txid of txids){
                console.log(txid)
                tx = await Message.findOne({'data.id': txid, type: 'groupMessage'}).sort({time: -1});
                chatmap.groups[txid] = tx;
            }

        }
    }

    ctx.body = chatmap || {
        address: ctx.params.account,
        contacts: {},
        groups:{}
    };
});

router.get('/public/group/member/:txid', async (ctx, next) => {

    let messages = await Message.find({text: 'addPublicGroup: ' + ctx.params.txid})

    ctx.body = messages.map(tx => tx.from);
});

router.get('/chatmap/:account', async (ctx, next) => {

    let chatmap = await ChatMap.findOne({ address: ctx.params.account});
    let groupid = 'blacknet1d3wnqk7h0fvq5j8mtvs4zr2crecavlsx6fyfma87lncgnah8vnesge8m3e';
    let queries = { to: groupid, type: 'chat', amount: 1000000};
    let groupLastMessage = await Message.findOne(queries).sort({time: -1});
    let groups = {};
    if(groupLastMessage){
        groups[groupid] = {
            time: groupLastMessage.time,
            message: groupLastMessage.text,
            sender: groupLastMessage.from,
            amount: groupLastMessage.amount
        }
    }else{
        groups[groupid] = {};
    }
    let data = {
        contacts: {}
    };

    if(chatmap) {
        data = chatmap.toObject();
    }

    data.groups = groups;

    ctx.body = data || {
        address: ctx.params.account,
        contacts: {},
        groups:{}
    };
});

router.get('/message/findone/:type', async (ctx, next) => {

    let type = ctx.params.type, queries = {type};

    queries = {
        ...ctx.query,
        type
    }

    let ret = await Message.findOne(queries);

    ctx.body = ret||{};
});

// for message query
router.get('/message/type/:type', async (ctx, next) => {

    let type = ctx.params.type, queries = {type};
    let page = ctx.query.page || 1;
    let query = ctx.query;


    for(let key in query){
        let value = query[key]

        if(key == 'page') continue;

        if(key.indexOf('data.') === 0){
            queries[key] = value;
        }else{
            queries[key] = value;
        }
    }

    ctx.body = await Message.find(queries).skip((page - 1) * 100).sort({ time: -1 }).limit(100);
});

router.get('/messages/:account', async (ctx, next) => {

    let to = ctx.query.with, page = ctx.query.page || 1, from = ctx.params.account;
    let queries = { $or: [{msgOwner: [from, to]}, {msgOwner: [to, from]}], type: 'chat'};
    let messages = await Message.find(queries).skip((page - 1) * 100).sort({ time: -1 }).limit(100);
    ctx.body = messages;
});

const developFundAddress = 'blacknet1d3wnqk7h0fvq5j8mtvs4zr2crecavlsx6fyfma87lncgnah8vnesge8m3e';


router.get('/messages/group/:groupid', async (ctx, next) => {

    let groupid = ctx.params.groupid, page = ctx.query.page || 1;
    let queries = { to: groupid, type: 'chat', amount: 1000000};
    let messages = await Message.find(queries).skip((page - 1) * 100).sort({ time: -1 }).limit(100);
    console.log(queries)
    ctx.body = messages;
});

const child_process = require('child_process');

router.post("/upload", async ctx => {
    const req = ctx.request;
    const cid = await exec(req.files.photo.path)
    
	ctx.body = {
		cid
	}
});

async function exec(path){

    return new Promise((resolve, reject)=>{

        child_process.exec(`ipfs add -q ${path}`, function(error, stdout, stderr){

            if(!error){
                let cid = stdout.replace(/[\n\t]/, '');
                resolve(cid);
            }
            reject(null);
        })
    })

}