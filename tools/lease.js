


require('../lib/mongo');
const API = require('../lib/api');

async function update() {
    
    let index = 0;

    console.log('reset all lease balance');

    let res = await Account.updateMany({}, {$set: {outLeasesBalance: 0, inLeasesBalance: 0, realBalance: 0}});
    console.log(res)
    while(true){
        let txns = await Transaction.find({$or: [{ type: 2 }, { type: 3 }]}).skip( index*100).limit(100).sort({ time: -1 }).lean();;

        console.log(`No.${index} page`);
        if(txns.length == 0) break;

        for(let tx of txns){
            await updateBalance(tx);
        }
        index++;
    }

    console.log('process complete');

    process.exit(0);
}


async function updateBalance(tx) {

    let fromAccount, toAcccount;

    fromAccount = await Account.findOne({address: tx.from});
    toAcccount  = await Account.findOne({address: tx.to});

    if(!fromAccount || !toAcccount) return;
    
    if(tx.type == 2){
        
        fromAccount.outLeasesBalance += tx.data.amount;
        toAcccount.inLeasesBalance += tx.data.amount;
        fromAccount.realBalance = fromAccount.outLeasesBalance + fromAccount.balance;
    }

    if(tx.type == 3){
        
        fromAccount.outLeasesBalance -= tx.data.amount;
        toAcccount.inLeasesBalance -= tx.data.amount;
        fromAccount.realBalance = toAcccount.outLeasesBalance + toAcccount.balance;
    }

    if(fromAccount.realBalance == 0){
        fromAccount.realBalance = fromAccount.balance;
    }

    if(toAcccount.realBalance == 0){
        toAcccount.realBalance = toAcccount.balance;
    }


    await fromAccount.save();
    await toAcccount.save();
}


update();