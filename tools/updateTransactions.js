
require('../lib/mongo');
const API = require('../lib/api');

async function update() {

    let index = 0;

    while (true) {

        let txns = await Transaction.find({"txowner": {$exists: false} }).skip(index * 1000).limit(1000);
        if (txns.length == 0) break;
        console.log(index)
        
        for (let tx of txns) {

            if (!tx.txowner || tx.txowner.length == 0) {
		if(tx.to || tx.from){
                tx.txowner = tx.to ? [tx.from, tx.to] : [tx.from];
                await tx.save();
		}
            }

        }
        index++;
    }

    process.exit(0);
}




update();